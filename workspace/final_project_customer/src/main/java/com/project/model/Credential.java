package com.project.model;

public class Credential {
	private String mail;
	private String password;
	public Credential(String mail, String password) {
		this.mail = mail;
		this.password = password;
	}
	public String getEmail() {
		return mail;
	}
	public void setEmail(String email) {
		this.mail = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
