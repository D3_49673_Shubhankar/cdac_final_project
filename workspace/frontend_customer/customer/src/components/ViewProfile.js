import { useSelector } from 'react-redux';
import axios from 'axios';
import { useState, useEffect } from 'react';
import { url } from './../constants/url';
import { Link } from 'react-router-dom';

const ViewProfile = ()=>{
    const id = useSelector((state) => state.loggedUser.cid)
    
    const [customer, setCustomer] =useState([])
    const getcustomer = ()=> {
        axios.get(url + `customers/${id}`)
        .then((response)=>{
            setCustomer(response.data)
        })
    }
    
    useEffect(() =>{
        getcustomer()
    }, [])
    
    if(id === 0){
        return (
            <div>
                please login first
            </div>
        )
    } else {
        
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs- col-sm- col-md- col-lg-">
                        <div className = "viewprofile">
                            <div className = "viewprofile-item">
                                First Name: {customer.firstName}
                            </div>
                            <div className = "viewprofile-item">
                                Last Name: {customer.lastName}
                            </div>
                            <div className = "viewprofile-item">
                                Phone Number: {customer.phone}    
                            </div>
                            <div className = "viewprofile-item">
                                Wallet Balnce:  {customer.walletBalance}
                            </div>
                            <div className = "viewprofile-item">
                                Date of Birth: {customer.bod}
                            </div>
                            <div className = "viewprofile-item">
                                Address : {customer.landmark}, {customer.city}, {customer.state}, {customer.pin}
                            </div>
                           <Link to ='/update-profile'>
                            <div className = "viewprofile-item">
                            <button className = "btn btn-primary">update</button>
                           </div>
                           </Link>
                        
                        </div>
                        
                    </div>
                </div>
            </div>
        )
    }
    
}

export default ViewProfile