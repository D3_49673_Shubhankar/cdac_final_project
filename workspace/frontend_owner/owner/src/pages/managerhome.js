import { History } from "history"
import { useHistory } from 'react-router-dom';
const ManagerHome = ()=> {
    let history=useHistory()
    return (
        <div>
            <div>
            <button onClick={()=>
            {
                history.push("view-products")
            }}
            className="btn btn-success">
                    View Products
            </button>
            </div>

            <div>
            <button onClick={()=>
            {
                history.push("view-customers")
            }}
            className="btn btn-success">
             View Customers     
            </button>
            </div>

           
            <div>
            <button onClick={()=>
            {
                history.push("add-category")
            }}
            className="btn btn-success">
                    Add Category
            </button>
            </div>

            <div>
            <button onClick={()=>
            {
                history.push("add-product")
            }}
            className="btn btn-success">
                    Add Product
            </button>
            </div>

            <div>
            <button onClick={()=>
            {
                history.push("view-orders-manager")
            }}
            className="btn btn-success">
             View Orders     
            </button>
            </div>

            <div>
            <button onClick={()=>
            {
                history.push("view-reviews")
            }}
            className="btn btn-success">
             View Reviews     
            </button>
            </div>

            <div>
            <button onClick={()=>
            {
                history.push("view-customised")
            }}
            className="btn btn-success">
             View Customised Product Requests     
            </button>
            </div>

           
        </div>
    )
}

export default ManagerHome