import { useHistory } from "react-router"
const AdminHome = ()=> {

    let history=useHistory()
    return (
        <div>
               <h1>Admin Dashboard</h1>
               <div>
            <button onClick={()=>
            {
                history.push("view-customers")
            }}
            className="btn btn-success">
             View Customers     
            </button>
            </div>

            <div>
            <button onClick={()=>
            {
                history.push("view-staff")
            }}
            className="btn btn-success">
             View Staff     
            </button>
            </div>

            <div>
            <button onClick={()=>
            {
                history.push("add-staff")
            }}
            className="btn btn-success">
             Add Staff    
            </button>
            </div>
        
        </div>
    )
}

export default AdminHome